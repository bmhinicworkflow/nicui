import os
import json
import datetime 
import csv 
import time
try:
    # Python 2
    from cStringIO import StringIO
except ImportError:
    # Python 3
    from io import StringIO

from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import redirect
from django.template import loader

from .forms import ConversionForm
from .forms import CorrelationForm
from .forms import CorrelationInputFormSet
from .forms import PatientDemoForm
from .forms import AdjacencyMatrixFormset
from .forms import NetworkAnalysisForm

import requests
from django.conf import settings
from django.conf import settings

from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt

from requests_toolbelt import MultipartEncoder

from django.shortcuts import render_to_response
from django.template import RequestContext

# landing page with demographics
@login_required(login_url=settings.DECORATOR_LOGIN_URL)
def landing(request):
    template = loader.get_template('nicinterface/landing-patient-demographics.html')
    get_patient_demo_URL = settings.CSFWEBSERVICES_BASE_URL + "patientdemo/get"

    headers = {'Content-Type': 'application/json'}
    patient_demo_obj = json.dumps({
        "username": request.user.get_username(),
    })

    patient_demo_response = requests.post(get_patient_demo_URL,
        data=patient_demo_obj,
        headers=headers,        
    )

    patient_demo_output = []
    patient_demo_server_error = ""

    if patient_demo_response.status_code == requests.codes.ok:
        patient_demo_output = patient_demo_response.json()
    else:
        patient_demo_server_error = "Server Error"
    patient_demo_form = PatientDemoForm()
    context = {
        'patientdemo_form' : patient_demo_form,
        'past_patient_demo_data': patient_demo_output,
        "patient_demo_server_error" : patient_demo_server_error

    }
    return HttpResponse(template.render(context, request))


@login_required(login_url=settings.DECORATOR_LOGIN_URL)
def conversion(request):
    template = loader.get_template('nicinterface/main-conversion.html')
    form = ConversionForm()
    patient_id = ""
    if 'patient_id' in request.session:
        patient_id = request.session['patient_id']
    
    context = {
        "form":form,
        "patient_id":patient_id,
    }
    return HttpResponse(template.render(context, request))    


@login_required(login_url=settings.DECORATOR_LOGIN_URL)
def conversion_form_process(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = ConversionForm(request.POST)
        edf_file_path_name = "N/A"
        # check whether it's valid:
        if form.is_valid():
            # result = django_rq.enqueue(count_words_at_url, 'http://nvie.com')

            # add workflow to server
            filepath = settings.TAVERNA_WORKFLOW_FILE_BASE + "conversion_workflow_final_status.t2flow"
            with open(filepath) as fh:
                headers = {'Content-Type': 'application/vnd.taverna.t2flow+xml'}
                mydata = fh.read()
                addworkflow_response = requests.post(settings.TAVERNA_BASE_URL,
                            data=mydata,
                            headers=headers,
                            auth=('taverna', 'taverna'),
                        )
            print("addworkflow_response: ", addworkflow_response.status_code)
            if addworkflow_response.status_code == requests.codes.created:

                # get run UUID
                print("HEADERS: ", addworkflow_response.headers)
                addworkflow_url = addworkflow_response.headers.get("Location")
                index = addworkflow_url.rfind("/");
                runID = addworkflow_url[index + 1:]
                print(runID)

                # add json library
                json_jar_loc = settings.TAVERNA_WORKFLOW_FILE_BASE + "javax.json-1.0.4.jar"
                addjar_response = ""
                with open(json_jar_loc, "rb") as jarf:
                    headers = {'Content-Type': 'application/octet-stream'}
                    mydata = jarf.read()
                    addjar_url = settings.TAVERNA_BASE_URL + runID + "/wd/lib/javax.json-1.0.4.jar"
                    addjar_response = requests.put(addjar_url,
                                data=mydata,
                                headers=headers,
                                auth=('taverna', 'taverna'),
                            )

                print("addjar_response: ", addjar_response)

                # add input directory
                headers = {'Content-Type': 'application/xml'}
                addInputDir_url = settings.TAVERNA_BASE_URL + runID +"/input/input/EDF_directory"
                xml_payload = """<t2sr:runInput xmlns:t2sr="http://ns.taverna.org.uk/2010/xml/server/rest/">
                                <t2sr:value>""" + form.cleaned_data['edf_file_loc'] + """</t2sr:value>
                                </t2sr:runInput>"""
                addedfdir_response = requests.put(addInputDir_url,
                            headers=headers,
                            data=xml_payload,
                            auth=('taverna', 'taverna'),
                        )

                print("addedfdir_response: ", addedfdir_response)

                # add input directory
                headers = {'Content-Type': 'application/xml'}
                addEpochDuration_url = settings.TAVERNA_BASE_URL + runID +"/input/input/Epoch_Duration"
                xml_payload = """<t2sr:runInput xmlns:t2sr="http://ns.taverna.org.uk/2010/xml/server/rest/">
                                <t2sr:value>""" + str(form.cleaned_data['epoch_duration']) + """</t2sr:value>
                                </t2sr:runInput>"""
                addEpochDuration_response = requests.put(addEpochDuration_url,
                            headers=headers,
                            data=xml_payload,
                            auth=('taverna', 'taverna'),
                        )

                print("addEpochDuration_response: ", addEpochDuration_response)

                # add input directory
                headers = {'Content-Type': 'application/xml'}
                epochSegment_url = settings.TAVERNA_BASE_URL + runID +"/input/input/Epochs_Per_Segment"
                xml_payload = """<t2sr:runInput xmlns:t2sr="http://ns.taverna.org.uk/2010/xml/server/rest/">
                                <t2sr:value>""" + str(form.cleaned_data['epochs_per_segment']) + """</t2sr:value>
                                </t2sr:runInput>"""
                epochSegment_response = requests.put(epochSegment_url,
                            headers=headers,
                            data=xml_payload,
                            auth=('taverna', 'taverna'),
                        )

                print("epochSegment_response: ", epochSegment_response)

                # add input directory
                headers = {'Content-Type': 'application/xml'}
                addOutputDir_url = settings.TAVERNA_BASE_URL + runID +"/input/input/Output_Directory"
                xml_payload = """<t2sr:runInput xmlns:t2sr="http://ns.taverna.org.uk/2010/xml/server/rest/">
                                <t2sr:value>""" + form.cleaned_data['output_csf_loc'] + """</t2sr:value>
                                </t2sr:runInput>"""
                addOutputDir_response = requests.put(addOutputDir_url,
                            headers=headers,
                            data=xml_payload,
                            auth=('taverna', 'taverna'),
                        )                                

                print("addOutputDir_response: ", addOutputDir_response)

                # get provnenace bundle
                prov_url = settings.TAVERNA_BASE_URL + runID + "/generate-provenance"
                headers = {'Content-Type': 'text/plain'}
                request_body = "true"
                prov_response = requests.put(prov_url,
                            headers=headers,
                            data=request_body,
                            auth=('taverna', 'taverna'),
                        )    

                print("prov_response: ", prov_response)                                    

                # before we start taverna run, make sure everything got set up correctly
                if addjar_response.status_code == requests.codes.created and \
                    addedfdir_response.status_code == requests.codes.ok and \
                    addEpochDuration_response.status_code == requests.codes.ok and \
                    epochSegment_response.status_code == requests.codes.ok and \
                    addOutputDir_response.status_code == requests.codes.ok and \
                    prov_response.status_code == requests.codes.ok:


                    # start taverna run
                    startWorkflow_url = settings.TAVERNA_BASE_URL + runID + "/status"
                    headers = {'Content-Type': 'text/plain'}
                    request_body = "Operating"
                    startWorkflow_response = requests.put(startWorkflow_url,
                                headers=headers,
                                data=request_body,
                                auth=('taverna', 'taverna'),
                            )                                

                    print("startWorkflow_response: ", startWorkflow_response)
                    print("startWorkflow_response: ", startWorkflow_response.headers)

                    # add previous run database entry
                    edf_conv_prov_URL = settings.CSFWEBSERVICES_BASE_URL + "conversionsave/insert"

                    headers = {'Content-Type': 'application/json'}
                    patient_demo_obj = json.dumps({
                        "run_uuid": runID,
                        "date_started" : datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                        "username": request.user.get_username(),
                        "edf_file_loc":form.cleaned_data['edf_file_loc'],
                        "output_file_loc":form.cleaned_data['output_csf_loc'],
                        "epoch_duration":form.cleaned_data['epoch_duration'],
                        "epoch_per_segment":form.cleaned_data['epochs_per_segment'],
                        "completed": False,
                        "patient_id":form.cleaned_data['conv_patient_mrn'],
                    })

                    edf_conv_prov_response = requests.post(edf_conv_prov_URL,
                        data=patient_demo_obj,
                        headers=headers,        
                    )

                    print("edf_conv_prov_response: ", edf_conv_prov_response)
                    # save edf file location for status update
                    edf_file_dir = form.cleaned_data['edf_file_loc']
                    request.session['edf_file_dir'] = edf_file_dir
                        # redirect to a new URL:
                    return redirect('form-conversion-thanks')   
                else:
                    patient_id = ""
                    if 'patient_id' in request.session:
                        patient_id = request.session['patient_id']

                    return render(request, 'nicinterface/main-conversion.html', 
                        {
                            'form': form,
                            "patient_id":patient_id,
                            "conversion_server_error": True
                        }
                    )
            else:

                patient_id = ""
                if 'patient_id' in request.session:
                    patient_id = request.session['patient_id']

                return render(request, 'nicinterface/main-conversion.html', 
                    {
                        'form': form,
                        "patient_id":patient_id,
                        "conversion_server_error": True
                    }
                )

    # if a GET (or any other method) we'll create a blank form
    else:
        form = ConversionForm()

    patient_id = ""
    if 'patient_id' in request.session:
        patient_id = request.session['patient_id']

    return render(request, 'nicinterface/main-conversion.html', 
        {
            'form': form,
            "patient_id":patient_id,
        }
    )

@login_required(login_url=settings.DECORATOR_LOGIN_URL)
def conversion_form_redirect(request):

    if 'edf_file_dir' not in request.session:
        return redirect('conversion')

    edf_file_dir = request.session['edf_file_dir']

    headers = {'Content-Type': 'application/json'}
    conversion_status_url = settings.CSFWEBSERVICES_BASE_URL + "conversionstatus"
    status_file_name = os.path.basename(os.path.normpath(edf_file_dir))
    status_file_full_path = edf_file_dir + "/edfconvstatus-" + status_file_name + ".tmp"

    conversion_status = requests.post(conversion_status_url,
                headers=headers,
                data=json.dumps({"statusFile":status_file_full_path})
            )
    
    status_file = "N/A"
    status_time = "N/A"
    status_step = "N/A"
    fraction_complete = "0"
    curr_image = "conv_workflow_final_nostatus_start.svg"

    if not 'fraction_complete' in request.session:
        request.session['fraction_complete'] = "0"

    if conversion_status.status_code != requests.codes.ok:
        status_server_error = False
        if conversion_status.content.decode("utf-8") != "Status File Not Found":
            status_server_error = True

        image_path = settings.STATIC_URL + "nicinterface/images/" + curr_image
        template = loader.get_template('nicinterface/conversionformthanks.html')
        context = {
            "curr_file":status_file,
            "curr_time":status_time,
            "curr_step":status_step,
            "fraction_complete" : fraction_complete,
            "status_workflow_img":image_path,
            "status_server_error":status_server_error,
        }
        return HttpResponse(template.render(context, request))    

    status_json = {}

    try:
        status_json = conversion_status.json()
    except json.decoder.JSONDecodeError:
        print("There is an error decoding JSON in Conversion Form")

    if status_json:
        # print("status json", status_json)
        status_file = status_json['curredfFile']
        status_time = status_json['currdate']
        status_step = status_json['currstep']
        fraction_complete = status_json['fractionComplete']
        # print("fraction_complete'", fraction_complete)

    top_and_bottom = fraction_complete.split("/")
    if fraction_complete != "N/A":
        # fraction_complete = str(round(float(top_and_bottom[0]) / float(top_and_bottom[1]) * 100.0, 2))
        fraction_complete = str(round(float(0) / float(1) * 100.0, 2))
        request.session['fraction_complete'] = fraction_complete

    print("status step: " , status_step)
    if status_step == "STUDY/CLINICAL-EVENT-METADATA-START" or status_step == "START-CONVERSION":
        curr_image = "conv_workflow_final_nostatus_start.svg"
    elif status_step == "STUDY/CLINICAL-EVENT-METADATA-END" or status_step == "CHANNEL-START":
        curr_image = "conv_workflow_final_nostatus_studymetadata_complete.svg"
    elif status_step == "CHANNEL-END":
        curr_image = "conv_workflow_final_nostatus_channelmetadata_complete.svg"
    elif status_step == "EDF-FRAGMENT-START":
        curr_image = "conv_workflow_final_nostatus_fragment_incomplete.svg"
    elif status_step == "CHANNEL-END" or status_step == "END-CONVERSION":
        curr_image = "conv_workflow_final_nostatus_fragment_complete.svg"

    image_path = settings.STATIC_URL + "nicinterface/images/" + curr_image
    template = loader.get_template('nicinterface/conversionformthanks.html')
    context = {
        "curr_file":status_file,
        "curr_time":status_time,
        "curr_step":status_step,
        "fraction_complete" : request.session['fraction_complete'],
        "status_workflow_img":image_path
    }
    return HttpResponse(template.render(context, request))    


# correlation workflow
@login_required(login_url=settings.DECORATOR_LOGIN_URL)
def correlation(request):
    template = loader.get_template('nicinterface/main-correlation.html')
    correlation_form = CorrelationForm()
    formset = CorrelationInputFormSet()

    patient_id = ""
    if 'patient_id' in request.session:
        patient_id = request.session['patient_id']
    print("patient_id: " , patient_id)

    context = {
        'correlation_form': correlation_form,
        'formset': formset,
        "patient_id":patient_id,
        'csfwebservices_url': settings.CSFWEBSERVICES_BASE_URL,
    }
    return HttpResponse(template.render(context, request))    


@login_required(login_url=settings.DECORATOR_LOGIN_URL)
@csrf_exempt
def correlation_inputs_session_store(request):
    if request.method == 'GET':
            return redirect('correlation')

    elif request.method == "POST":

        request.session['corr_metrics'] = request.POST['corr_metrics']
        # request.session['corr_output_loc'] = request.POST['corr_output_loc']
        request.session['corr_patient_id'] = request.POST['corr_patient_id']
        request.session['corr_run_uuid'] = request.POST['corr_run_uuid']
        request.session['csf_file_loc'] = request.POST['csf_file_loc']

        resp = {"success" : True}
        return HttpResponse(resp)


@login_required(login_url=settings.DECORATOR_LOGIN_URL)
def correlation_prepopulated(request):

    corr_seiz_input_url = settings.CSFWEBSERVICES_BASE_URL + "correlationsave/getseizureinputs"
    
    headers = {'Content-Type': 'application/json'}

    corr_seiz_input_response = requests.post(corr_seiz_input_url,
                data=json.dumps({"run_uuid":request.session['corr_run_uuid']}),
                headers=headers,
            )

    corr_seiz_prefill_inputs = []
    if corr_seiz_input_response.status_code == requests.codes.ok:
        corr_seiz_prefill_inputs = corr_seiz_input_response.json()

    
    template = loader.get_template('nicinterface/main-correlation.html')
    correlation_form = CorrelationForm()
    formset = CorrelationInputFormSet()
    patient_demo_form = PatientDemoForm()


    context = {
        'correlation_form': correlation_form,
        'formset': formset,
        'patientdemo_form' : patient_demo_form,
        "corr_seiz_prefill_inputs": corr_seiz_prefill_inputs
    }
    
    return HttpResponse(template.render(context, request))


@login_required(login_url=settings.DECORATOR_LOGIN_URL)
def correlation_form_process(request):
    if request.method == 'GET':
        correlation_form = CorrelationForm(request.GET or None)
        formset = CorrelationInputFormSet()
    # if this is a POST request we need to process the form data
    elif request.method == 'POST':
        # create a form instance and populate it with data from the request:
        correlation_form = CorrelationForm(request.POST)
        formset = CorrelationInputFormSet(request.POST)
        # check whether it's valid:

        # print("Oh boy1: ", correlation_form)
        if correlation_form.is_valid() and formset.is_valid():
            # result = django_rq.enqueue(count_words_at_url, 'http://nvie.com')

            #set up the form lists first
            seizure_start_time_string = formset.cleaned_data[0]['seizure_start_time'].strftime("%d.%m.%y,%H.%M.%S")
            seizure_end_time_string = formset.cleaned_data[0]['seizure_end_time'].strftime("%d.%m.%y,%H.%M.%S")
            lag_window_string = str(formset.cleaned_data[0]['lag_window'])
            channel_list_string = formset.cleaned_data[0]['channel_list']
            corr_output_loc_string = formset.cleaned_data[0]['corr_output_loc']

            for seizure_window_param in formset.cleaned_data[1:]:
                seizure_start_time_string += ";" + seizure_window_param['seizure_start_time'].strftime("%d.%m.%y,%H.%M.%S")
                seizure_end_time_string += ";" + seizure_window_param['seizure_end_time'].strftime("%d.%m.%y,%H.%M.%S")
                lag_window_string += ";" + str(seizure_window_param['lag_window'])
                channel_list_string += ";" + seizure_window_param['channel_list']
                corr_output_loc_string += ";" + seizure_window_param['corr_output_loc']
            

            # add workflow to server
            filepath = settings.TAVERNA_WORKFLOW_FILE_BASE + "correlation_workflow_final_status.t2flow"
            with open(filepath) as fh:
                headers = {'Content-Type': 'application/vnd.taverna.t2flow+xml'}
                mydata = fh.read()
                addworkflow_response = requests.post(settings.TAVERNA_BASE_URL,
                            data=mydata,
                            headers=headers,
                            auth=('taverna', 'taverna'),
                        )

            if addworkflow_response.status_code == requests.codes.created:

                # get run UUID
                print("addworkflow_response.headers", addworkflow_response.headers)
                addworkflow_url = addworkflow_response.headers.get("Location")
                index = addworkflow_url.rfind("/");
                runID = addworkflow_url[index + 1:]
                print("runID: " , runID)

                # add json library
                json_jar_loc = settings.TAVERNA_WORKFLOW_FILE_BASE + "javax.json-1.0.4.jar"
                addjar_response = ""
                with open(json_jar_loc, "rb") as jarf:
                    headers = {'Content-Type': 'application/octet-stream'}
                    mydata = jarf.read()
                    addjar_url = settings.TAVERNA_BASE_URL + runID + "/wd/lib/javax.json-1.0.4.jar"
                    addjar_response = requests.put(addjar_url,
                                data=mydata,
                                headers=headers,
                                auth=('taverna', 'taverna'),
                            )

                print("addjar_response: ", addjar_response)

                # add input directory
                headers = {'Content-Type': 'application/xml'}
                addInputDir_url = settings.TAVERNA_BASE_URL+ runID +"/input/input/CSF_File_Directory"
                xml_payload = """<t2sr:runInput xmlns:t2sr="http://ns.taverna.org.uk/2010/xml/server/rest/">
                                <t2sr:value>""" + correlation_form.cleaned_data['csf_file_loc'] + """</t2sr:value>
                                </t2sr:runInput>"""
                addcsfDir_response = requests.put(addInputDir_url,
                            headers=headers,
                            data=xml_payload,
                            auth=('taverna', 'taverna'),
                        )

                print("addcsfDir_response: ", addcsfDir_response)

                # add correlation measures
                formatted_corr_list = ",".join(correlation_form.cleaned_data['corr_metric_list'])
                print("formatted_corr_list: ", formatted_corr_list)

                headers = {'Content-Type': 'application/xml'}
                addInputDir_url = settings.TAVERNA_BASE_URL+ runID +"/input/input/Correlation_Measures"
                xml_payload = """<t2sr:runInput xmlns:t2sr="http://ns.taverna.org.uk/2010/xml/server/rest/">
                                <t2sr:value>""" + formatted_corr_list + """</t2sr:value>
                                </t2sr:runInput>"""
                addcorrMeasure_response = requests.put(addInputDir_url,
                            headers=headers,
                            data=xml_payload,
                            auth=('taverna', 'taverna'),
                        )

                # add seizure start times
                headers = {'Content-Type': 'application/xml'}
                addInputDir_url = settings.TAVERNA_BASE_URL+ runID +"/input/input/Seizure_Start_Time"
                xml_payload = """<t2sr:runInput xmlns:t2sr="http://ns.taverna.org.uk/2010/xml/server/rest/">
                                <t2sr:value>""" + seizure_start_time_string + """</t2sr:value>
                                </t2sr:runInput>"""
                addSzStartTime_response = requests.put(addInputDir_url,
                            headers=headers,
                            data=xml_payload,
                            auth=('taverna', 'taverna'),
                        )

                print("seizure_start_time_string: ", seizure_start_time_string)

                # add seizure end times
                headers = {'Content-Type': 'application/xml'}
                addInputDir_url = settings.TAVERNA_BASE_URL+ runID +"/input/input/Seizure_End_Time"
                xml_payload = """<t2sr:runInput xmlns:t2sr="http://ns.taverna.org.uk/2010/xml/server/rest/">
                                <t2sr:value>""" + seizure_end_time_string + """</t2sr:value>
                                </t2sr:runInput>"""
                addSzEndTime_response = requests.put(addInputDir_url,
                            headers=headers,
                            data=xml_payload,
                            auth=('taverna', 'taverna'),
                        )

                print("seizure_end_time_string: ", seizure_end_time_string)



                # add channel lists
                headers = {'Content-Type': 'application/xml'}
                addInputDir_url = settings.TAVERNA_BASE_URL+ runID +"/input/input/Channel_List"
                xml_payload = """<t2sr:runInput xmlns:t2sr="http://ns.taverna.org.uk/2010/xml/server/rest/">
                                <t2sr:value>""" + channel_list_string + """</t2sr:value>
                                </t2sr:runInput>"""
                addChannelList_response = requests.put(addInputDir_url,
                            headers=headers,
                            data=xml_payload,
                            auth=('taverna', 'taverna'),
                        )

                print("channel_list_string: ", channel_list_string)

                # add lag windows
                headers = {'Content-Type': 'application/xml'}
                addInputDir_url = settings.TAVERNA_BASE_URL+ runID +"/input/input/Lag_Window"
                xml_payload = """<t2sr:runInput xmlns:t2sr="http://ns.taverna.org.uk/2010/xml/server/rest/">
                                <t2sr:value>""" + lag_window_string + """</t2sr:value>
                                </t2sr:runInput>"""
                addLagWindow_response = requests.put(addInputDir_url,
                            headers=headers,
                            data=xml_payload,
                            auth=('taverna', 'taverna'),
                        )

                print("lag_window_string: ", lag_window_string)

                # add output file path
                headers = {'Content-Type': 'application/xml'}
                addInputDir_url = settings.TAVERNA_BASE_URL+ runID +"/input/input/Output_File_Path"
                xml_payload = """<t2sr:runInput xmlns:t2sr="http://ns.taverna.org.uk/2010/xml/server/rest/">
                                <t2sr:value>""" + corr_output_loc_string + """</t2sr:value>
                                </t2sr:runInput>"""
                addOutputPath_response = requests.put(addInputDir_url,
                            headers=headers,
                            data=xml_payload,
                            auth=('taverna', 'taverna'),
                        )

                print("corr_output_loc_string: ", corr_output_loc_string)

                # generate provenance bundle
                prov_url = settings.TAVERNA_BASE_URL + runID + "/generate-provenance"
                headers = {'Content-Type': 'text/plain'}
                request_body = "true"
                prov_response = requests.put(prov_url,
                            headers=headers,
                            data=request_body,
                            auth=('taverna', 'taverna'),
                        )    

                print("prov_response: ", prov_response)                                    

                if addjar_response.status_code == requests.codes.created and \
                    addcsfDir_response.status_code == requests.codes.ok and \
                    addcorrMeasure_response.status_code == requests.codes.ok and \
                    addSzStartTime_response.status_code == requests.codes.ok and \
                    addSzEndTime_response.status_code == requests.codes.ok and \
                    addChannelList_response.status_code == requests.codes.ok and \
                    addLagWindow_response.status_code == requests.codes.ok and \
                    addOutputPath_response.status_code == requests.codes.ok and \
                    prov_response.status_code == requests.codes.ok:

                    # start workflow
                    startWorkflow_url = settings.TAVERNA_BASE_URL + runID + "/status"
                    headers = {'Content-Type': 'text/plain'}
                    request_body = "Operating"
                    startWorkflow_response = requests.put(startWorkflow_url,
                                headers=headers,
                                data=request_body,
                                auth=('taverna', 'taverna'),
                            )                                

                    print("startWorkflow_response: ", startWorkflow_response)

                    # add previous run database entry
                    csf_corr_prov_URL = settings.CSFWEBSERVICES_BASE_URL + "correlationsave/insert"

                    headers = {'Content-Type': 'application/json'}
                    csf_corr_main_obj = json.dumps({
                        "run_uuid": runID,
                        "date_started" : datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                        "username": request.user.get_username(),
                        "csf_file_loc": correlation_form.cleaned_data['csf_file_loc'],
                        "corr_metrics": formatted_corr_list,
                        "completed": False,
                        "patient_id": correlation_form.cleaned_data['corr_patient_mrn'],
                    })

                    csf_corr_prov_response = requests.post(csf_corr_prov_URL,
                        data=csf_corr_main_obj,
                        headers=headers,        
                    )

                    csf_seiz_prov_URL = settings.CSFWEBSERVICES_BASE_URL + "correlationsave/insertseizures"
                    headers = {'Content-Type': 'application/json'}

                    all_seizure_inputs = []
                    for seizure_window_param in formset.cleaned_data:
                        all_seizure_inputs.append({
                            "run_uuid" : runID,
                            "seizure_start": seizure_window_param['seizure_start_time'].strftime("%Y-%m-%d %H:%M:%S"),
                            "seizure_end": seizure_window_param['seizure_end_time'].strftime("%Y-%m-%d %H:%M:%S"),
                            "lag_window": str(seizure_window_param['lag_window']),
                            "channel_list" : seizure_window_param['channel_list'],
                            "corr_output_loc": seizure_window_param['corr_output_loc'],
                        })

                    csf_seiz_prov_response = requests.post(csf_seiz_prov_URL,
                        data=json.dumps(all_seizure_inputs),
                        headers=headers,        
                    )

                    print("csf_seiz_prov_response: " , csf_seiz_prov_response.text)


                    request.session['csf_file_loc'] = correlation_form.cleaned_data['csf_file_loc']

                    return redirect('form-correlation-thanks')
                else:
                    patient_id = ""
                    if 'patient_id' in request.session:
                        patient_id = request.session['patient_id']

                    return render(request, 
                        'nicinterface/main-correlation.html',
                        {
                            'correlation_form': correlation_form,
                            "patient_id":patient_id,
                            'formset': formset,
                            "correlation_server_error": True
                        })

            else:
                patient_id = ""
                if 'patient_id' in request.session:
                    patient_id = request.session['patient_id']

                return render(request, 
                    'nicinterface/main-correlation.html',
                    {
                        'correlation_form': correlation_form,
                        "patient_id":patient_id,
                        'formset': formset,
                        "correlation_server_error": True
                    })


    else:
        correlation_form = CorrelationForm(request.GET or None)
        formset = CorrelationInputFormSet()

    patient_id = ""
    if 'patient_id' in request.session:
        patient_id = request.session['patient_id']

    return render(request, 
        'nicinterface/main-correlation.html',
        {
            'correlation_form': correlation_form,
            "patient_id":patient_id,
            'formset': formset
        })


@login_required(login_url=settings.DECORATOR_LOGIN_URL)
def correlation_form_redirect(request):

    if 'csf_file_loc' not in request.session:
        return redirect('correlation')

    csf_file_dir = request.session['csf_file_loc']
    headers = {'Content-Type': 'application/json'}
    correlation_status_url = settings.CSFWEBSERVICES_BASE_URL + "correlationstatus"
    status_file_name = os.path.basename(os.path.normpath(csf_file_dir))
    status_file_full_path = csf_file_dir + "/edfcorrstatus-" + status_file_name + ".tmp"

    correlation_status = requests.post(correlation_status_url,
                headers=headers,
                data=json.dumps({"statusFile": status_file_full_path})
            )
    
    status_file = "N/A"
    status_time = "N/A"
    status_step = "N/A"
    curr_image = "correlation_workflow_final_nostatus.svg"

    status_seizure_start = "N/A"
    status_seizure_end = "N/A"
    status_lag_window = "N/A"
    status_channel_list = "N/A"

    status_json = {}

    if correlation_status.status_code != requests.codes.ok:
        status_server_error = False
        if correlation_status.content.decode("utf-8") != "Status File Not Found":
            status_server_error = True

        image_path = settings.STATIC_URL + "nicinterface/images/" + curr_image
        template = loader.get_template('nicinterface/correlationformthanks.html')
        context = {
            "curr_file":status_file,
            "curr_time":status_time,
            "curr_step":status_step,
            "curr_seizure_start": status_seizure_start,
            "curr_seizure_end": status_seizure_end,
            "curr_lag_window": status_lag_window,
            "curr_channel_list":status_channel_list,
            "status_workflow_img":image_path,
            "status_server_error":status_server_error
        }
        return HttpResponse(template.render(context, request))    
    
    else:
        try:
            status_json = correlation_status.json()
        except json.decoder.JSONDecodeError:
            print("There is an error decoding JSON in Correlation Form")

        if status_json:
            print("status json", status_json)
            status_file = status_json['currCSFFile']
            status_time = status_json['currdate']
            status_step = status_json['currstep']

            status_seizure_start = status_json['currseizurestart']
            status_seizure_end = status_json['currseizureend']
            status_lag_window = status_json['currlagwindow']
            status_channel_list = status_json['currchannellist']

        
        if status_step == "CORRELATION-START":
            curr_image = "correlation_workflow_final_nostatus_phase_start.svg"
        elif status_step == "PHASE-START":
            curr_image = "correlation_workflow_final_nostatus_phase_start.svg"
        elif status_step == "PHASE-END":
            curr_image = "correlation_workflow_final_nostatus_phase_end.svg"
        elif status_step == "PEARSON-START":
            curr_image = "correlation_workflow_final_nostatus_pearson_start.svg"
        elif status_step == "PEARSON-END":
            curr_image = "correlation_workflow_final_nostatus_pearson_end.svg"
        elif status_step == "PIJN-START":
            curr_image = "correlation_workflow_final_nostatus_pijn_start.svg"
        elif status_step == "PIJN-END":
            curr_image = "correlation_workflow_final_nostatus_pijn_end.svg"
        elif status_step == "CORRELATION-END":
            curr_image = "correlation_workflow_final_nostatus_end.svg"

        image_path = settings.STATIC_URL + "nicinterface/images/" + curr_image
        template = loader.get_template('nicinterface/correlationformthanks.html')
        context = {
            "curr_file":status_file,
            "curr_time":status_time,
            "curr_step":status_step,
            "curr_seizure_start": status_seizure_start,
            "curr_seizure_end": status_seizure_end,
            "curr_lag_window": status_lag_window,
            "curr_channel_list":status_channel_list,
            "status_workflow_img":image_path
        }
        return HttpResponse(template.render(context, request))    

# patient demographics form process
@login_required(login_url=settings.DECORATOR_LOGIN_URL)
def patient_demo_process(request):
    if request.POST:
        form = PatientDemoForm(request.POST, request.FILES)

        if form.is_valid():
            # save data or whatever other action you want to take
            file_obj = form.cleaned_data['discharge_sum']

            m = MultipartEncoder(
                fields={'username': request.user.get_username(),
                        'file': (file_obj.name, file_obj.read(), 'application/pdf')}
                )
            # files = {'file': file_obj.read()}
            patient_discharge_summary_URL = settings.CSFWEBSERVICES_BASE_URL + "discharge/file"
            # r = requests.post(patient_discharge_summary_URL, data=m)
            discharge_summary_response = requests.post(patient_discharge_summary_URL, data=m,
                              headers={'Content-Type': m.content_type})

            if discharge_summary_response.status_code == requests.codes.ok:

                patient_demo_summary_URL = settings.CSFWEBSERVICES_BASE_URL + "patientdemo/insert"

                print("discharge: " , discharge_summary_response)
                headers = {'Content-Type': 'application/json'}
                patient_demo_obj = json.dumps({
                    "patient_id":form.cleaned_data['patient_mrn'],
                    "username":request.user.get_username(),
                    "epi_zone_locale":form.cleaned_data['epi_zone_locale'],
                    "seiz_semiology":form.cleaned_data['seiz_semiology'],
                    "seiz_etiology":form.cleaned_data['seiz_etiology'],
                    "seiz_freq":form.cleaned_data['seiz_freq'],
                    "med_conds":form.cleaned_data['med_conds'],
                    "pt_age":form.cleaned_data['pt_age'],
                    "pt_gender":form.cleaned_data['pt_gender'],
                    "discharge_sum_link":discharge_summary_response.json()['dischargePath']
                })

                patient_demo_response = requests.post(patient_demo_summary_URL,
                    data=patient_demo_obj,
                    headers=headers,        
                )

                if patient_demo_response.status_code == requests.codes.ok:
                    request.session['patient_id'] = form.cleaned_data['patient_mrn']
            else:
                # TODO: do something for the user to indicate we dind't success
                resp = {'success': False}

            resp = {'success': True}

        else:
           resp = {'success': False}

        return HttpResponse(resp)

    else:
        return HttpResponseBadRequest()


# network analysis page
@login_required(login_url=settings.DECORATOR_LOGIN_URL)
def network_analysis(request):
    template = loader.get_template('nicinterface/network-analysis.html')
    network_analysis_form = NetworkAnalysisForm()
    # this form is to take the input from the clinician of the description (e.g. ictal event 1) of each correlation measure output file they include in the formset
    adjacency_matrix_form = AdjacencyMatrixFormset()
    context = {
        'network_analysis_form': network_analysis_form,
        "formset":adjacency_matrix_form,
        "base_url": settings.CSFWEBSERVICES_BASE_URL

    }
    
    return HttpResponse(template.render(context, request))


@login_required(login_url=settings.DECORATOR_LOGIN_URL)
def network_analysis_process(request):
    if request.method == 'GET':
        network_analysis_form = NetworkAnalysisForm(request.GET or None)
        adjacency_matrix_formset = AdjacencyMatrixFormset()
    # if this is a POST request we need to process the form data
    elif request.method == 'POST':
        # create a form instance and populate it with data from the request:
        network_analysis_form = NetworkAnalysisForm(request.POST)
        adjacency_matrix_formset = AdjacencyMatrixFormset(request.POST)
        # check whether it's valid:
        if network_analysis_form.is_valid() and adjacency_matrix_formset.is_valid():
            time.sleep(2)
            # print("network_analysis_form", network_analysis_form.cleaned_data)
            network_analysis_by_sz_event = {}
            for seizure_event in adjacency_matrix_formset.cleaned_data:
                
                seizure_event_na_output = perform_network_analysis(seizure_event['corr_output_loc'], 
                    network_analysis_form.cleaned_data['network_analysis_list'],
                    network_analysis_form.cleaned_data['correlation_measure_list'])
                network_analysis_by_sz_event[seizure_event['seizure_event_name']] = seizure_event_na_output
            # output of the form:
            # {"test1" : {"pijn":[], "phase":[]}, "test2":{..., ...}}
            
            # reformat the above format so the events are divided by corr measure
            network_analysis_by_corr_measure = {}
            random_idx = next(iter(network_analysis_by_sz_event))
            corr_measures = network_analysis_by_sz_event[random_idx].keys()

            for measure in corr_measures:
                network_analysis_by_corr_measure[measure] = {}

                for sz_name in network_analysis_by_sz_event.keys():
                    network_analysis_by_corr_measure[measure][sz_name] = network_analysis_by_sz_event[sz_name][measure]

            analysis_output_filename = network_analysis_form.cleaned_data['network_analysis_output_file']

            metadata_output_list = []
            for key, value in network_analysis_by_sz_event.items():
                metadata_output_dict = {}
                metadata_output_dict['Seizure Event Name'] = key
                metadata_dict = value[next(iter(value))][0]
                if "OutputFile" in metadata_dict:
                    metadata_output_dict['Input Directory'] = metadata_dict['InputDirectory']
                    metadata_output_dict['Output File'] = metadata_dict['OutputFile']
                    metadata_output_dict['Channels'] = metadata_dict['Channels']
                    metadata_output_dict['Start Time'] = metadata_dict["StartTime"]
                    metadata_output_dict['End Time'] = metadata_dict["EndTime"]
                    metadata_output_list.append(metadata_output_dict)

            response_data = {}
            response_data['network_analysis_by_corr_measure'] = network_analysis_by_corr_measure
            response_data['metadata_output_list'] = metadata_output_list
            response_data['network_analysis_output_file'] = analysis_output_filename
            response_data['network_analysis_by_sz_event'] = network_analysis_by_sz_event
            return HttpResponse(json.dumps(response_data), content_type="application/json")

        else:
            return redirect("network-analysis")
    else:
        network_analysis_form = NetworkAnalysisForm(request.GET or None)
        adjacency_matrix_formset = AdjacencyMatrixFormset()

    return redirect("network-analysis")


# this function returns an output of the form:
# {"pearson":[{sz1 na1}, {sz1 na2}], "pijn":[{sz1 na1}, {sz1 na2}]}
# for as many network analysis metrics as are supplied and whichever corr measures were calculated in the file provided
# all for ONE seizure event
def perform_network_analysis(corr_output_file, network_analysis_metric_list, correlation_measure_list):
    network_analysis_output = []
    for measure in network_analysis_metric_list:
        if measure == "charpathlength":
            charpathlength_URL = settings.CSFWEBSERVICES_BASE_URL + "charpathlength"
            headers = {'Content-Type': 'application/json'}
            
            charpathlength_response = requests.post(charpathlength_URL,
                data=json.dumps({"corrMeasuresFile": corr_output_file}),
                headers=headers)
            print(charpathlength_response)
            if charpathlength_response.status_code == requests.codes.ok:
                network_analysis_output.append(charpathlength_response.json())

        elif measure == "degreedist":
            degreedist_URL = settings.CSFWEBSERVICES_BASE_URL + "degreedistro"
            headers = {'Content-Type': 'application/json'}
            
            degreedist_response = requests.post(degreedist_URL,
                data=json.dumps({"corrMeasuresFile": corr_output_file}),
                headers=headers)
            # print(degreedist_response.json())
            if degreedist_response.status_code == requests.codes.ok:
                network_analysis_output.append(degreedist_response.json())

        elif measure == "globaleff":
            globaleff_URL = settings.CSFWEBSERVICES_BASE_URL + "globalefficiency"
            headers = {'Content-Type': 'application/json'}
            
            globaleff_response = requests.post(globaleff_URL,
                data=json.dumps({"corrMeasuresFile": corr_output_file}),
                headers=headers)
            # print(globaleff_response.json())
            if globaleff_response.status_code == requests.codes.ok:
                network_analysis_output.append(globaleff_response.json())

        elif measure == "numcliques":
            numcliques_URL = settings.CSFWEBSERVICES_BASE_URL + "numcliques"
            headers = {'Content-Type': 'application/json'}
            
            numcliques_response = requests.post(numcliques_URL,
                data=json.dumps({"corrMeasuresFile": corr_output_file}),
                headers=headers)
            # print(globaleff_response.json())
            if numcliques_response.status_code == requests.codes.ok:
                network_analysis_output.append(numcliques_response.json())

    network_analysis_output_removed_measures = []
    for idx1, na_measure_data in enumerate(network_analysis_output):
        network_analysis_output_removed_measures.append([])
        for idx2, indv_val in enumerate(na_measure_data):
            if indv_val["Measures"] in correlation_measure_list:
                network_analysis_output_removed_measures[idx1].append(indv_val)

    reformatted_na_output = map(list, zip(*network_analysis_output_removed_measures))
    network_analysis_by_corr_measure = {}
    for na_output_list in reformatted_na_output:
        corr_measure = na_output_list[0]['Measures']
        network_analysis_by_corr_measure[corr_measure] = na_output_list

    return network_analysis_by_corr_measure


@login_required(login_url=settings.DECORATOR_LOGIN_URL)
@csrf_exempt
def download_network_analysis(request):
    if request.method == 'GET':
        return redirect("network-analysis")
    # if this is a POST request we need to process the form data
    elif request.method == 'POST':
        data = json.loads(request.body)

        network_analysis_by_corr_measure = data['data']
        network_analysis_by_sz_event = data['network_analysis_by_sz_event']


        output = StringIO()

        # first we will write the metadata
        for key, value in network_analysis_by_sz_event.items():
            output.write("Seizure Name: " + key + "\n")
            metadata_dict = value[next(iter(value))][0]
            if "OutputFile" in metadata_dict:
                output.write("InputDirectory:" + metadata_dict['InputDirectory'] + "\n")
                output.write("OutputFile:" + metadata_dict['OutputFile'] + "\n")
                output.write("Channels:" + metadata_dict['Channels'] + "\n")
                output.write("StartTime:" + metadata_dict["StartTime"] + "\n")
                output.write("EndTime:" + metadata_dict["EndTime"] + "\n")
                output.write("\n")

        output.write("\n--\n")

        for corr_measure, na_analysis_by_szname in network_analysis_by_corr_measure.items():
            if corr_measure == "PHASE":
                output.write("Correlation Measure:" + corr_measure + " (https://doi.org/10.1016/S0167-2789(00)00087-7)\n")
            elif corr_measure == "PIJN":
                output.write("Correlation Measure:" + corr_measure + " (https://doi.org/10.1007/978-1-4612-0341-4_4)\n")
            elif corr_measure == "PEARSON":    
                output.write("Correlation Measure:" + corr_measure + " (ISBN: 9780128047538)\n")

            # create the matrix 

            #header first
            sz_names = list(na_analysis_by_szname.keys())
            output.write("X," + ",".join(sz_names))
            output.write("\n")

            # matrix output
            num_na_metrics = len(na_analysis_by_szname[sz_names[0]])
            for na_idx in range(0, num_na_metrics):
                na_key_val = ""
                if "degreedist" in na_analysis_by_szname[sz_names[0]][na_idx]:
                    output.write("dd,")
                    na_key_val = "degreedist"
                if "globaleff" in na_analysis_by_szname[sz_names[0]][na_idx]: 
                    output.write("ge,")
                    na_key_val = "globaleff"
                if "cpl" in na_analysis_by_szname[sz_names[0]][na_idx]: 
                    output.write("cpl,")
                    na_key_val = "cpl"
                if "numCliques" in na_analysis_by_szname[sz_names[0]][na_idx]: 
                    output.write("numCliques,")
                    na_key_val = "numCliques"

                for sz_name in sz_names:
                    output.write(str(na_analysis_by_szname[sz_name][na_idx][na_key_val]) + ",")
                output.write("\n")
                na_key_val = ""
            output.write("\n\n")

        response = HttpResponse(content_type='text/plain')
        response.write(output.getvalue())
        response['Content-Disposition'] = 'attachment; filename='+ data['filename']
        response['Content-Length'] = output.tell()
        return response
    

# provenance capture 
@login_required(login_url=settings.DECORATOR_LOGIN_URL)
def past_run_provenance(request):
    
    edf_run_prov_URL = settings.CSFWEBSERVICES_BASE_URL + "conversionsave/getall"
    
    headers = {'Content-Type': 'application/json'}
    edf_run_response = requests.post(edf_run_prov_URL,
        data=json.dumps({"username": request.user.get_username()}),
        headers=headers)

    csf_run_prov_URL = settings.CSFWEBSERVICES_BASE_URL + "correlationsave/getall"
    csf_run_response = requests.post(csf_run_prov_URL,
        data=json.dumps({"username": request.user.get_username()}),
        headers=headers)

    edf_run_output = []
    csf_run_output = []

    if edf_run_response.status_code == requests.codes.ok:
        edf_run_output = edf_run_response.json()
    if csf_run_response.status_code == requests.codes.ok:
        csf_run_output = csf_run_response.json()

    template = loader.get_template('nicinterface/past-run-history.html')
    context = {
        'past_edf_run_data': edf_run_output,
        'past_csf_run_data': csf_run_output
    }
    
    return HttpResponse(template.render(context, request))


# error handling pages
# python 2.7
# def handler404(request, *args, **argv):
#     response = render_to_response('404.html', {},
#                                   context_instance=RequestContext(request))
#     response.status_code = 404
#     return response


# def handler500(request, *args, **argv):
#     response = render_to_response('500.html', {},
#                                   context_instance=RequestContext(request))
#     response.status_code = 500
#     return response

# python 3.5
def handler404(request, exception, template_name="nicinterface/404.html"):
    return render(request, 'nicinterface/404.html', status=404)

def handler500(request, template_name="nicinterface/500.html"):
    return render(request, 'nicinterface/500.html', status=500)
