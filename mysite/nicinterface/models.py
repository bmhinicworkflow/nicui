from django.db import models

class ConversionHistory(models.Model):
 
  your_name = models.TextField(max_length=100)
  edf_file_loc = models.TextField()
  # edf_file_loc = forms.FilePathField(path="./", allow_folders=True)
  # see diff?
  # output_csf_loc = forms.FilePathField(path="./")
  output_csf_loc = models.TextField()
  epoch_duration = models.IntegerField()
  epochs_per_segment = models.IntegerField()


class CorrelationHistory(models.Model):

  CORRELATION_METRIC_CHOICES = (
    ('pijn', 'PIJN Correlation'),
    ('pearson', 'Pearson Correlation'),
    ('phase', 'Phase Coherence Correlation'),
  )    

  csf_file_loc = models.TextField()
  
  # correlation form elements
  #pijn', 'pearson', 'phase' as a comma-delimited list
  corr_metric_list = models.CharField(max_length=25)
  # corr_output_loc = forms.FilePathField(path="./", allow_folders=True)
  corr_output_loc = models.TextField()


class CorrelationTimeHistory(models.Model):
  # Format: '16.05.14,16.10.10'  
  seizure_start_time = models.DateTimeField()
  seizure_end_time = models.DateTimeField()
  lag_window = models.DecimalField(max_digits=50, decimal_places=2)
  channel_list = models.TextField()
  correlation = models.ForeignKey(CorrelationHistory, on_delete=models.CASCADE)
