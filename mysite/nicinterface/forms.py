from django import forms
from django.forms import BaseFormSet
from django.forms import formset_factory
from django.core.validators import RegexValidator

class ConversionForm(forms.Form):
 
  conv_patient_mrn = forms.IntegerField(label="Patient ID (Medical Record Number)", widget=forms.TextInput(attrs={'placeholder': '12345'}))
  edf_file_loc = forms.CharField(label='Path to EDF Files', strip=True, widget=forms.TextInput(attrs={'placeholder': '/path/to/edf/directory'}))
  # edf_file_loc = forms.FilePathField(path="./", allow_folders=True)
  # see diff?
  # output_csf_loc = forms.FilePathField(path="./")
  output_csf_loc = forms.CharField(label='Desired Full Output Path of CSF Files', strip=True, widget=forms.TextInput(attrs={'placeholder': '/path/to/csf/output/directory'}))
  epoch_duration = forms.IntegerField()
  epochs_per_segment = forms.IntegerField()


class CorrelationInputForm(forms.Form):
    # create an extra set of fields
    
    # Format: '16.05.14,16.10.10'  
    # Should ALWAYS be day, month, year for CSFwebservices to work
    seizure_start_time = forms.DateTimeField(input_formats=['%d.%m.%y,%H.%M.%S',], 
      label="Event Start Time", widget=forms.TextInput(attrs={'placeholder': 'e.g. 16.03.20,10.15.05'}))

    seizure_end_time = forms.DateTimeField(input_formats=['%d.%m.%y,%H.%M.%S',], 
      label="Event End Time", widget=forms.TextInput(attrs={'placeholder': 'e.g. 16.03.20,10.15.45'}))
    
    lag_window = forms.DecimalField(decimal_places=2, label="Lag Window (in seconds)", 
      widget=forms.TextInput(attrs={'placeholder': 'e.g. 0.10'}))
    
    channel_list = forms.CharField(label="List of Channels (comma-separated)", strip=True, 
      widget=forms.TextInput(attrs={'placeholder': 'e.g. lk1,lk3,lj1'}))
    
    corr_output_loc = forms.CharField(label='Path to Correlation Metrics Output File', strip=True, 
      widget=forms.TextInput(attrs={'placeholder': '/output/path/of/measures.txt'}), validators=[RegexValidator(regex=r"\.txt$", message="Must be a .txt file")])


class BaseCorrelationInputFormSet(BaseFormSet):
  def clean(self):
      """Checks that no two correlation inputs have the same output file loc."""
      if any(self.errors):
          # Don't bother validating the formset unless each form is valid on its own
          return
      corr_output_locs = []
      for form in self.forms:
          corr_output_loc = form.cleaned_data['corr_output_loc']
          if corr_output_loc in corr_output_locs:
              raise forms.ValidationError("Each seizure window must have its own output file.")
          corr_output_locs.append(corr_output_loc)  


CorrelationInputFormSet = formset_factory(CorrelationInputForm, formset=BaseCorrelationInputFormSet, extra=1)


class CorrelationForm(forms.Form):
  CORRELATION_METRIC_CHOICES = (
    ('pijn', 'PIJN Correlation'),
    ('pearson', 'Pearson Correlation'),
    ('phase', 'Phase Coherence Correlation'),
  )    

  corr_patient_mrn = forms.IntegerField(label="Patient ID (Medical Record Number)", widget=forms.TextInput(attrs={'placeholder': '12345'}))
  csf_file_loc = forms.CharField(label='Full Path of CSF Files', strip=True,  widget=forms.TextInput(attrs={'placeholder': '/path/to/csf/directory'}))
  # correlation form elements
  corr_metric_list = forms.MultipleChoiceField(choices=CORRELATION_METRIC_CHOICES, widget=forms.CheckboxSelectMultiple)
  # corr_output_loc = forms.FilePathField(path="./", allow_folders=True)


class PatientDemoForm(forms.Form):
  patient_mrn = forms.IntegerField(label="Patient ID (Medical Record Number)")
  epi_zone_locale = forms.CharField(widget=forms.Textarea, label="Localization of Epileptic Zone")
  seiz_semiology = forms.CharField(widget=forms.Textarea, label="Seizure Semiology")
  seiz_etiology = forms.CharField(widget=forms.Textarea, label="Etiology")
  seiz_freq = forms.CharField(label="Seizure Frequency")
  med_conds = forms.CharField(widget=forms.Textarea, label="Related Medical Conditions")
  pt_age = forms.IntegerField(label="Patient Age")
  pt_gender = forms.CharField(label="Patient Gender")
  discharge_sum = forms.FileField(label="Upload Patient Discharge Summary")

class NetworkAnalysisForm(forms.Form):

  NETWORK_ANALYSIS_MEASURES = (
    ('degreedist', 'Degree Distribution'),
    ('charpathlength', 'Characteristic Path Length'),
    ('globaleff', 'Global Efficiency'),
    ('numcliques', 'Number of Cliques'),    
  )

  CORRELATION_MEASURES = (
    ('PHASE', 'Phase Coherence Correlation'),
    ('PIJN', 'Nonlinear Correlation Coefficient'),
    ('PEARSON', "Pearson's Coefficient"),
  )


  network_analysis_list = forms.MultipleChoiceField(choices=NETWORK_ANALYSIS_MEASURES, widget=forms.SelectMultiple)
  correlation_measure_list = forms.MultipleChoiceField(choices=CORRELATION_MEASURES, widget=forms.SelectMultiple)
  network_analysis_output_file = forms.CharField(label='Network Analysis Output File Name (.txt)', strip=True,  
    widget=forms.TextInput(attrs={'placeholder': 'e.g. na_file.txt'}), validators=[RegexValidator(regex=r"\.txt$", message="Must be a .txt file")])

class AdjacencyMatrixForm(forms.Form):

  seizure_event_name = forms.CharField(label='Name of Seizure Event', strip=True,  widget=forms.TextInput(attrs={'placeholder': 'e.g. ictal event 1'}))
  corr_output_loc = forms.CharField(label='Full Path of Correlation Measure File (.txt)', strip=True,  
    widget=forms.TextInput(attrs={'placeholder': '/path/to/correlation/file.txt'}), validators=[RegexValidator(regex=r"\.txt$", message="Must be a .txt file")])


AdjacencyMatrixFormset = formset_factory(AdjacencyMatrixForm, formset=BaseCorrelationInputFormSet, extra=1)


