from django.dispatch import receiver
from django.db.models.signals import pre_save
from django.db.models.signals import post_save
from django.contrib.auth.models import User

from django.conf import settings
import uuid

from django.core.mail import send_mail

@receiver(pre_save, sender=User)
def set_new_user_inactive(sender, instance, **kwargs):
    if instance._state.adding is True:
        print("Creating Inactive User")
        instance.is_active = False
    else:
        print("Updating User Record")


def email_admin_new_user(sender, instance, created, **kwargs):
    user = instance
    if created:
      message_body = 'Hello NIC Admin,\n\n' \
        'The following user has created an account: ' + user.username + '.' \
        'The user has the following email account: ' + user.email + "." \
        'Please login to the admin console, verify the user, and notify them.\n\n' \
        'Thank you'

      send_mail(
          'New NIC User Created',
          message_body,
          'nicworkflow@gmail.com',
          settings.ADMIN_EMAIL_LIST,
          fail_silently=False,
      )      
  
  

post_save.connect(email_admin_new_user, sender=User, dispatch_uid=str(uuid.uuid4()))