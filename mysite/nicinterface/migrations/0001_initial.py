# Generated by Django 2.1 on 2019-02-06 21:54

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ConversionHistory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('your_name', models.TextField(max_length=100)),
                ('edf_file_loc', models.TextField()),
                ('output_csf_loc', models.TextField()),
                ('epoch_duration', models.IntegerField()),
                ('epochs_per_segment', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='CorrelationHistory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('csf_file_loc', models.TextField()),
                ('corr_metric_list', models.CharField(max_length=25)),
                ('corr_output_loc', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='CorrelationTimeHistory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('seizure_start_time', models.DateTimeField()),
                ('seizure_end_time', models.DateTimeField()),
                ('lag_window', models.DecimalField(decimal_places=2, max_digits=50)),
                ('channel_list', models.TextField()),
                ('correlation', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='nicinterface.CorrelationHistory')),
            ],
        ),
    ]
