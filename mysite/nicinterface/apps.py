from django.apps import AppConfig


class NicinterfaceConfig(AppConfig):
    name = 'nicinterface'

    def ready(self):
        import nicinterface.signals