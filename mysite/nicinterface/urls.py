from django.urls import path

from . import views

urlpatterns = [
    path('', views.landing, name='landing'),
    path('conversion', views.conversion, name='conversion'),
    path('formconversion/', views.conversion_form_process, name="form-conversion"),
    path('conversionthanks/', views.conversion_form_redirect, name="form-conversion-thanks"),
    
    path('correlation', views.correlation, name='correlation'),
    path('correlationsession', views.correlation_inputs_session_store, name="correlation-session-store"),
    path('correlationprefilled', views.correlation_prepopulated, name="correlation-prefilled"),
    path("formcorrelation/", views.correlation_form_process, name="form-correlation"),
    path('correlationthanks/', views.correlation_form_redirect, name="form-correlation-thanks"),

    path('patientdemo/', views.patient_demo_process, name="form-patient-demo"),

    path('history/', views.past_run_provenance, name="history"),

    path('networkanalysis', views.network_analysis, name="network-analysis"),
    path('networkanalysisprocess', views.network_analysis_process, name="network-analysis-process"),
    path("downloadnetworkanalysis", views.download_network_analysis, name="download-network-analysis"),
    # path('nathanks', views.network_analysis_redirect, name="network-analysis-thanks"),
    # path('corr_history/', views.past_run_provenance, name="corr-provenance")
]

