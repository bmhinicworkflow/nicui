.. _provenance:

Provenance and Run History
==========================

Due to the lack of scientific reproducibility in biomedicine, in general, the NIC platform has implemented several features we hope will encourage reproducible science. In addition to a PostgreSQL database that stores all inputs, we also allow the user to recreate past runs. 


----------------------------------------------------------------------

After you have completed a conversion and correlation run, you are able to rerun one of these runs. 

.. NOTE::
   You will only be able to see your past runs and no one else can see your runs.


The page will look like this: 

.. image:: images/rerun.*   
 	:class: screenshot

When you click either `Rerun Conversion Input` or `Rerun Correlation Inputs`, you will be taken back to those respective pages with the forms automatically populated with that run's data. Then you can follow the process described in steps 2 or 3. 