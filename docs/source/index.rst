.. Neuro-Integrative Connectivity (NIC) documentation master file, created by
   sphinx-quickstart on Thu Apr 25 18:08:16 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Welcome to Neuro-Integrative Connectivity's documentation!
==========================================================


The Neuro-Integrative Connectivity (NIC) was developed to enable users with little computational experience to perform efficient network analysis on SEEG data to investigate epilepsy in patients. This documentation has been created to walk you through the steps necessary to calculate the supported network analysis measures and interpret the results as effortlessly as possible.

.. toctree::
   :maxdepth: 2
   :caption: Table of Contents
   :titlesonly:

   introduction
   step1
   step2
   step3
   step4
   history
   docker
   extras


How to Use NIC and this Documentation
-------------------------------------

There are two editions of the NIC system. 

1. NIC Server Edition (at https://bmhinformatics.case.edu/nicworkflow)

  The Server Edition is only for use with data on the CWRU servers in the Population and Quantitative Health Sciences department and UH Epilepsy Center.

2. NIC Docker Container (at https://hub.docker.com/r/vsocrates/nicworkflow)

  The Docker Container can be downloaded from the public repository and run on any platform with your own data. Additional instructions can be found at :ref:`installing-docker`.
