.. _step-2-label:

Step 2: Conversion from EDF-CSF Files
=====================================

The NIC system assumes that you start with **EDF** files from an epilepsy patient that you have collected data for. The first step is to convert these EDF files to our defined CSF file format. 

Our system only supports CSF files and we encourage their use as they provide support for human-readability and better management of your metadata (e.g. clinical seizure event annotations, EEG instrument and electrode details, study details). If you'd like to learn more information, please visit https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4360820/

------------------------------------------------------

This step is pretty simple. You will see a form that accepts a number of inputs described below: 

.. CAUTION::
   This form assumes that the input EDF folder pertains to only one patient and contains both the EDF files and the clinical annotations in .txt format. 

.. image:: images/conversion_form.* 
   :class: screenshot  

.. tabularcolumns:: |p{1cm}|p{6cm}|

.. csv-table:: Conversion Input Parameters
   :file: _files/conversion_inputs.csv
   :header-rows: 1
   :widths: 1 1


Once you click `Run Conversion` you will be taken to a status page that looks like this: 

.. image:: images/conversion_thanks.*
   :class: screenshot

This page will continue to update every few minutes with a status update. Eventually, you will see a popup that takes you to the next step, Calculation of Correlation Measures. 