.. _step-3-label:

Step 3: Calculation of Coupling Measures
========================================

In this step, we will be calculating coupling measures between electrodes to determine any temporal correlations. 

This allows us to create an adjacency matrix representation of a network graph that we can calcalate network summary statistics on. For more information regarding network graph representations, please check :ref:`network-graph-repr-label`.

We support three different correlation measures: the linear Pearson's Correlation, and non-linear mean phase coherence correlation, and the non-linear correlation coefficient (Pijn's Measure).


--------------------------------------------------------------------------------

When you get to the Correlation page, you will see the following form. This page works very similarly to the conversion page. 


.. image:: images/correlation_form.*   
   :class: screenshot

.. tabularcolumns:: |p{1cm}|p{6cm}|

.. csv-table:: Correlation Overall Input Parameters
   :file: _files/correlation_main_inputs.csv
   :header-rows: 1
   :widths: 1 1

Once you have selected the location of the CSF files (**for one patient**), you can select as many event windows as you'd like by clicking `Add Seizure Event` and `Remove`. 

.. csv-table:: Correlation Event Window Input Parameters
   :file: _files/correlation_sz_inputs.csv
   :header-rows: 1
   :widths: 1 1


Once you click `Run Correlations` you will be taken to a very similar screen that allows you to view your status as shown below: 

.. image:: images/correlation_thanks.* 
   :class: screenshot 

Again, a popup will take you to the next step, network analysis.  

