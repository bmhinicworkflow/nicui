.. _step-4-label:

Step 4: Calculation of Network Analysis Measures
================================================

 Once we calculate our coupling measures, we can create an :ref:`adjacency matrix representation<network-graph-repr-label>` to better draw conclusions about our coupled electrodes created in step 3. See more detailed information regarding these summary metrics at :ref:`network-analysis-summary-label`.

--------------------------------------------------------------------------------

 You will be taken to a form that looks like the following: 

 .. image:: images/network_analysis_form.*   
 	:class: screenshot

.. csv-table:: Network Analysis Inputs
   :file: _files/network_analysis_inputs.csv
   :header-rows: 1
   :widths: 1 1

You will fill out the network analysis form on the left side of the page and the outputs will be displayed on the right once they are calculated. You are also able to download a text/csv version of this output for your analysis later. 

.. TIP::
   Congratulations! You have successfully completed use of the NIC system.


There are a few other features such as :ref:`rerunning previous runs<provenance>` that you should feel free to play around with. 
