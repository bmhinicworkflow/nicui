Create an Account
=================

The first step is to create an account. If you are using the server edition of NIC, you will have to create an account on the signup page shown below. 

.. image:: images/signup.*
   :class: screenshot

Once you sign up, you will see a confirmation message like the one below. 

.. image:: images/signup_confirmation.*

Please wait while we verify your account. You should hear from us within a day or two. If not, please contact vxs215@case.edu.

Once your account is verified, you'll be able to move on to the next step. You should start with :ref:`step-1-label`.

---------------------------------------------------------------------

Overview of Steps
=================

Here, we will briefly outline the *major* steps that will be discussed in greater detail in further sections. Before these steps, we ask that you enter the **patient demographics** identified by the patient ID into our secure system.

Conversion from EDF-CSF Files
-----------------------------

In this **first** step, we convert the conventional _European Data Format (EDF)_ files that you probably have to our defined _Cloudwave Signal Format (CSF)_ files. 

For this step, please click :ref:`step-1-label`.

Calculation of Coupling Measures
--------------------------------

In this **second** step, once you have created and outputted CSF files, we can calulate coupling measures between electrodes. 

For this step, please click :ref:`step-2-label`.

Calculation of Network Analysis Measures
----------------------------------------

In this **third** step, we can calculate network analysis measures from the coupled electrodes in step 3. 

For this step, please click :ref:`step-3-label`.
